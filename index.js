// Requiring the http module

let http = require("http")

// Creating the server
// createServer() method

http.createServer(
	function(request, response) {
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Hello World from Batch 177')
	}
).listen(4000)

// Create a console log to indicate that the server is running

console.log('Server running at localhost: 4000')